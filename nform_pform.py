#!/usr/bin/env python 
# -*- coding: utf-8 -*-


from __future__ import division
import json
from pprint import pprint
import codecs
from collections import defaultdict
from operator import itemgetter
import fnmatch
import os
import gzip
from lxml import etree
from constit_tools import is_leaf, get_text, get_nps, top_structure, np_head, add_leaf_info, get_concept, get_postags

'''This script reads in sentence and parse tree information from json files. It extracts nounphrases and stores noun noun compounds (n-forms) and nouns followed by a prepositional phrase (p-forms) each in their own dictionary. The key of each phrase in the dictionary is composed of its primary and secondary head. Finally the keys of both dictionaries are compared to find alternating pairs, which are then written to the output json file. In the output, each concept is listed with is n-form and p-form and all instances of these with their locations. If here are occurences with additional modifiers, these are listed as subconcepts, also with their instances.'''

nn_h1h2 = {} #dictionary for storing the n-forms
pp_h1h2 = {} #dictionary for storing the p-forms
prep_fd = defaultdict(int) #keeping track of the prepositions that occur in p-forms and their frequencies
totnps = 0 #keeping count of the total number of noun phrases found in the data

matches = []
for root, dirnames, filenames in os.walk('../sample-data'): #this is a path on my computer, must be changed for pipeline
  for filename in fnmatch.filter(filenames, 'document-concepts-pre.json.gz'): #identify the relevant files
    matches.append(os.path.join(root, filename))  
print len(matches)
for path in matches[:500]: #iterate over the files (here I'm taking only half of them, the first 500)
  with gzip.open(path) as f: #open the zipped file
    data = json.load(f) # load the json data
  try:  
    trees = data["sentenceConstituents"] #get the constituency trees
    sentences = data["sentences"] #get the sentence information (for the tokens)
    doi = data["doi"] #get the document indentifier
  except KeyError: #the occasional file doesn't seem to have trees or sentences
    print path #print the path to the file, in case we want to investigate what's broken
  for sent in trees:	#for each constituency tree
    s_index = sent["sentenceIndex"] #look up the sentence index
    tokens = sentences[s_index]["tokens"] #use the sentence index to collect the token information
    nps = []
    get_nps(sent, nps) # extract all noun phrases from the sentence, collect them in a list
    for np in nps:
      totnps += 1
      try:
        pattern = top_structure(np, tokens) #inspect how the np is structured at the top level
# The block below extracts 'n-forms': noun - noun compounds.
# It looks for patterns that end in 'NN(P) NN(S)', i.e. a singular or plural noun, preceded by a singular noun (you don't normally find plurals in this position) or proper name.
# Noun phrase that have a conjunction (CC, 'and') at the top level are ignored. The nps embedded in them have also been extracted in the np extraction step and will be processed separately.
        if pattern[-1] in ['NN','NNS'] and pattern[-2] in ['NN', 'NNP'] and not 'CC' in pattern:
          head1 = np_head(np, tokens) #identify the primary head noun of the np. This will be the final noun.
          if head1: 
            head2 = np_head(np["children"][-2], tokens) #identify the secondary head, in this case the preceding noun
            if head2:
              mods = [] #extract additional modifiers if any (now only considering nouns, but we could add adjectives) Typically they precede and modify the secondary head. They may participate in the alternation: Mod H2 H1 -vs- H1 prep Mod H2 (user interaction semantics - semantics of user interaction)
              for c in np["children"][:-2]: #the final two children of the NP are the two nouns h2 and h1 that were already identified. Any previous children may be modifiers.
                if is_leaf(c):
                  token_index = c["tokenRange"]["begin"]
                  token = tokens[token_index]
                  pos = token["posValue"]
                  if pos in ['NN', 'NNP']:#, 'JJ', 'VBN', 'VBG']:
                    c["inConcept"] = "1" #mark this leaf as being part of the concept. This makes it easier to narrow down the text of the np to only the words that are part of the concept. Heads are also marked as being part of the concept, but this happens as part of hte function that identifies the head.
                  mods.append(get_text(c, tokens))#add the text form of the modifier to the list
              if not (head2, head1) in nn_h1h2: #create an entry for the pair of nouns if none is there yet
                nn_h1h2[(head2, head1)] = defaultdict(list)
              add_leaf_info(np, tokens) #add the information about the tokens to the tree
              indices = get_concept(np) #get the actual concept out of the np, discarding for example words like 'the'. Find out where the actual concept begins and ends
              begin = indices[0]
              end = indices[-1]
              text = ' '.join([t["text"] for t in tokens[begin:end+1]]) #get the text of the concept
              tokenRange = {"begin":begin, "end":end} #the tokenRange of the concept is used as part of the 'location' to indicate where the concept is found in the text. The other ingredient of the 'location' information are the sentence index and the document ID (doi)
              location = {"tokenRange":tokenRange, "sentenceIndex":s_index, "doi":doi}
              nn_h1h2[(head2, head1)][' '.join(mods)].append({"npText":get_text(np, tokens), "posTags":get_postags(np, tokens), "conceptText":text, "location":location}) #for each modifier (or modifier combination) that occurs we store a list of all instances that have that modifier. We inlcude the text (concptText) of these instances and its location
              #npText and posTags are for verification and debugging purposes. It can be useful to look at the complete NP to see if what was extracted is sensible, if any modifiers were missed, etc.
# The block below extracts 'p-forms', nouns that are followed by a prepositional phrase that modifies it.
# For now we are only using the most frequent and reliable pattern" : 'NP PP'
        elif pattern == ['NP', 'PP']:
          head1 = np_head(np, tokens) #identify the primary head noun of the np. This will be the noun before the PP
          if head1:
            pp = np["children"][-1]
            head2 = np_head(pp, tokens) #identify the secondary head, inside of the PP
            p_index = pp["tokenRange"]["begin"] #start extracting the prepostition (It will be part of the p-form)
            prep = tokens[p_index]["lemma"]
            pos = tokens[p_index]["posValue"]
            if pos in ['IN', 'TO', 'VBN', 'VBG']: #Most prepositions are postagged 'IN', 'to' has its own tag, VBN is a past participle, and VBG is a gerund (ing-form). 
              if tokens[p_index+1]["posValue"] == 'IN': #captures combinations of 2 words where the second is a preposition (e.g. 'filled with', 'related to)
                preptext = '_'.join([tokens[p_index]["text"], tokens[p_index+1]["text"]])
                #no good way yet to capture to capture phrasal preps, like 'in relation to'. Using a general pattern Prep Noun Prep generates too much noise.
              else:
                preptext = tokens[p_index]["text"] #Otherwise the preposition is just the first word of the PP
              prep_fd[preptext] += 1 #keep count of the frequencies of prepostitions
              if head2 and not (head2 in ['equation'] or head1 in ['equation']) and not preptext in ['than', 'since', 'following', 'outside']: #filter out some noise. These prepositions were (upon inspection) found to not yield valid alternations.
                mods = []
                sub_nps = []
                get_nps(pp, sub_nps) #extract nps from within the pp to find modifiers. The PP is composed of a preposition and an NP. This NP may be complex, containing other NPs.
                if len(sub_nps) > 1:
                  main_np = sub_nps[1] #if there are embedded NPs, look into the first one of these ([0] is always the embedding NP, since the np finding funtion starts at the top and then dives down.)
                else: main_np = sub_nps[0] #when there are no embedded NPs
                if "children" in main_np:
                  for c in main_np["children"][:-1]: #the last child in the np is the secondary head. Any previous children may be modifiers. They are extracted as above.
                    if is_leaf(c):
                      token_index = c["tokenRange"]["begin"]
                      token = tokens[token_index]
                      pos = token["posValue"]
                      if pos in ['NN', 'NNP']:#, 'JJ', 'VBN', 'VBG']:
                        c["inConcept"] = "1"
                        mods.append(get_text(c, tokens)) 
                if not (head2, head1) in pp_h1h2:
                  pp_h1h2[(head2, head1)] = {} #create an entry for the pair
                if not preptext in pp_h1h2[(head2, head1)]: 
                  pp_h1h2[(head2, head1)][preptext] = defaultdict(list)#create an entry for each proposition that occurs with the pair (usually just one, but sometimes there are two different ones (result of/from trial))
                add_leaf_info(np, tokens)
                indices = get_concept(np)
                begin = indices[0]
                end = indices[-1]
                text = ' '.join([t["text"] for t in tokens[begin:end+1]])
                tokenRange = {"begin":begin, "end":end}
                location = {"tokenRange":tokenRange, "sentenceIndex":s_index, "doi":doi}
                pp_h1h2[(head2, head1)][preptext][' '.join(mods)].append({"npText":get_text(np, tokens), "posTags":get_postags(np, tokens), "conceptText":text, "location":location}) #file all of the information away (same as above)
      except IndexError: #catches a couple of problems not all of which well understood
        break 

print totnps #total number of nps extracted
print len([pair for pair in nn_h1h2 if pair in pp_h1h2]) #total number of alternating pairs
#print [p for p in sorted(prep_fd.items(), key=itemgetter(1), reverse=True)]

altpreps = defaultdict(int) #count prepositions as they occur in actual alternations

#create json structure for the results:
root = {"concepts":[]}
for pair in nn_h1h2:
  if pair in pp_h1h2: #list pairs that occur in both forms (n-form and p-form)
    concept = {}
    root["concepts"].append(concept)
    concept["nForm"] = ' '.join(pair) #list the n-form
    for prep in pp_h1h2[pair]:
      altpreps[prep] += 1
      p = prep.title()
      concept["pForm"+p] = ' '.join([pair[1], prep, pair[0]]) #list the p-forms
    if nn_h1h2[pair]['']: #if there are instances without modifiers, list them under the main concept:
      concept["nInstances"] = []
      for phrase in nn_h1h2[pair]['']:
        ex = {}
        concept["nInstances"].append(ex) # the n-form instances
        ex["text"] = phrase
    for prep in pp_h1h2[pair]:
      p = prep.title()
      if pp_h1h2[pair][prep]['']:
        concept["pInstances"+p] = []
        for phrase in pp_h1h2[pair][prep]['']:
          ex = {}
          concept["pInstances"+p].append(ex) #the p-form instances (for each preposition)
          ex["text"] = phrase
      shared_mods = [] #check if there are any modifiers that occur with both n-form and p-form
      for mod in pp_h1h2[pair][prep]:
        if not mod == '':
          if mod in nn_h1h2[pair]:
            shared_mods.append(mod)
      if shared_mods:
        concept["subconcepts"] = []
        for mod in shared_mods: #make the concept with a shared modifier a subconcept
          sub = {}
          concept["subconcepts"].append(sub)
          sub["alternating"] = "1" #indicate that the modifier occurs with both forms
          sub["nForm"] = ' '.join([mod, pair[0], pair[1]])
          sub["pForm"+p] = ' '.join([pair[1], prep, mod, pair[0]])
          sub["nInstances"] = []
          for phrase in nn_h1h2[pair][mod]:
            ex = {}
            sub["nInstances"].append(ex)
            ex["text"] = phrase
          sub["pInstances"+p] = []
          for phrase in pp_h1h2[pair][prep][mod]:
            ex = {}
            sub["pInstances"+p].append(ex)
            ex["text"] = phrase  
    for mod in nn_h1h2[pair]: #similar for non-alternating modifiers
      if not mod in shared_mods and not mod == '':
        if not "subconcepts" in concept:
          concept["subconcepts"] = []
        sub = {}
        concept["subconcepts"].append(sub)
        sub["alternating"] = "0"
        sub["nForm"] = ' '.join([mod, pair[0], pair[1]])
        sub["nInstances"] = []
        for phrase in nn_h1h2[pair][mod]: #only n-form instances here
          ex = {}
          sub["nInstances"].append(ex)
          ex["text"] = phrase
    for prep in pp_h1h2[pair]:
      p = prep.title()
      for mod in pp_h1h2[pair][prep]:
        if not mod in shared_mods and not mod == '':
          if not "subconcepts" in concept:
            concept["subconcepts"] = []
          sub = {}
          concept["subconcepts"].append(sub)
          sub["alternating"] = "0"
          sub["pForm"+p] = ' '.join([pair[1], prep, mod, pair[0]])
          sub["pInstances"+p] = []
          for phrase in pp_h1h2[pair][prep][mod]: #only p-form instances here
            ex = {}
            sub["pInstances"+p].append(ex)
            ex["text"] = phrase  
        
with open('nform_pform.json', 'w') as f: #write it all to a file
     json.dump(root, f)

#print len([c for c in root.iter("Concept")])
#print len([ex for ex in root.iter("Ex")])

print [p for p in sorted(altpreps.items(), key=itemgetter(1), reverse=True)] #print the prepositions and their frequencies for inspection

    