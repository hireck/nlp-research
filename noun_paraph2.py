#!/usr/bin/env python 
# -*- coding: utf-8 -*-


from __future__ import division
import json
from pprint import pprint
import codecs
from collections import defaultdict
from operator import itemgetter
import fnmatch
import os
import gzip



def is_leaf(subtree):
  return subtree["tokenRange"]["begin"] == subtree["tokenRange"]["end"]


def flatten_tree(tree, tokens, printlist):
  if "type" in tree:
    printlist.extend(["(", tree["type"], ' '])
  elif is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    text = token["text"]
    pos = token["posValue"]
    printlist.extend(['(', pos, ' ', text, ' ', ')'])
  else: printlist.append('(')
  if "children" in tree:
    for c in tree["children"]:
      flatten_tree(c, tokens, printlist)
  printlist.append(')')  
  
  
def find_text(tree, tokens, text):
  if is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    text.append(token["text"])
  else:
    for c in tree["children"]:
      find_text(c, tokens, text)
      
def get_text(tree, tokens):
  text = []
  find_text(tree, tokens, text)
  return ' '.join(text)  

def add_leaf_info(tree, tokens, text, lemmas, postags, indices):
  if is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    form = token["text"]
    lemma = token["lemma"]
    postag = token["posValue"]
    tree["text"] = form
    tree["lemma"] = lemma
    tree["posValue"] = postag
    text.append(form)
    lemmas.append(lemma)
    postags.append(postag)
    indices.append(token_index)
  else:
    for c in tree["children"]:
      add_leaf_info(c, tokens, text, lemmas, postags, indices)
  
def get_leaf_info(tree, tokens):
  text = []
  lemmas = []
  postags = []
  indices = []
  add_leaf_info(tree, tokens, text, lemmas, postags, indices)
  tree["text"] = text
  tree["lemmas"] = lemmas
  tree["postags"] = postags
  tree["indices"] = indices
  
  
def get_nps(tree, nps):
  if "type" in tree and tree["type"]=="NP":
    nps.append(tree)
  elif "children" in tree:
    for c in tree["children"]:
      get_nps(c, nps)

      
def top_structure(np, tokens):
  labels = []
  #print np
  for c in np["children"]:
    #print get_text(c, tokens)
    if "type" in c:
      labels.append(c["type"])
    else:
      token_index = c["tokenRange"]["begin"]
      token = tokens[token_index]
      pos = token["posValue"]
      labels.append(pos)
  return labels
  
  
def np_head(np, tokens):
  #print get_text(np, tokens)
  if is_leaf(np):
    token_index = np["tokenRange"]["begin"]
    token = tokens[token_index]
    pos = token["posValue"]
    if pos.startswith('N'): 
      head = token["lemma"]
      return head
  else:
    for c in reversed(np["children"]):
      if is_leaf(c):
	token_index = c["tokenRange"]["begin"]
	token = tokens[token_index]
	pos = token["posValue"]
	if pos.startswith('N'): #DT for this, these, all?
	  #print 'head:', token["text"]
	  head = token["lemma"]
	  return head
      elif "type" in c and c["type"]=="NP":
	#print 'NP:', get_text(c, tokens)
	return np_head(c, tokens)
  


      
nn_h1h2 = defaultdict(list)
pp_h1h2 = {}
prep_fd = defaultdict(int)
totnps = 0
	
matches = []
for root, dirnames, filenames in os.walk('/media/data/unsilo/sample-data'):
  for filename in fnmatch.filter(filenames, 'document-concepts-pre.json.gz'):
    matches.append(os.path.join(root, filename))  
    #print os.path.join(root, filename)
print len(matches)
for path in matches[:500]:
  with gzip.open(path) as f:
    data = json.load(f)
  try:  
    trees = data["sentenceConstituents"]
    sentences = data["sentences"]
  except KeyError: 
    print path
  for sent in trees:	
    s_index = sent["sentenceIndex"]
    tokens = sentences[s_index]["tokens"]
    nps = []
    get_nps(sent, nps)
    for np in nps:
      totnps += 1
      #print get_text(np, tokens)
      try:
	pattern = top_structure(np, tokens)
	if (pattern[-1] == 'NN' or pattern[-1] == 'NNS') and pattern[-2] == 'NN':
	  head1 = np_head(np, tokens)
	  if head1:
	    head2 = np_head(np["children"][-2], tokens)
	    if head2:
	      nn_h1h2[(head2, head1)].append(get_text(np, tokens))
	elif pattern == ['NP', 'PP']:
	  head1 = np_head(np, tokens)
	  if head1:
	    pp = np["children"][-1]
	    #print pp
	    head2 = np_head(pp, tokens)
	    p_index = pp["tokenRange"]["begin"]
	    prep = tokens[p_index]["lemma"]
	    pos = tokens[p_index]["posValue"]
	    if pos in ['IN', 'TO', 'VBN', 'VBG']:
	      if tokens[p_index+1]["posValue"] == 'IN':
		preptext = '_'.join([tokens[p_index]["text"], tokens[p_index+1]["text"]])
	      else:
		preptext = tokens[p_index]["text"]
	      prep_fd[preptext] += 1
	      if head2 and not (head2 in ['equation'] and head1 == head2) and not preptext in ['than', 'since']:
		if not (head2, head1) in pp_h1h2:
		  pp_h1h2[(head2, head1)] = defaultdict(list)
		pp_h1h2[(head2, head1)][preptext].append(get_text(np, tokens))
		#print head1, head2, '|', get_text(np, tokens)
      except IndexError: 
	break 

print totnps
print len([pair for pair in nn_h1h2 if pair in pp_h1h2])
print [p for p in sorted(prep_fd.items(), key=itemgetter(1), reverse=True)]

altpreps = defaultdict(int)
	
outfile = codecs.open('alternations2.txt', 'w', 'utf-8')	
for pair in nn_h1h2:
  if pair in pp_h1h2:
    for prep in pp_h1h2[pair]:
      altpreps[prep] += 1
      #print pair, nn_h1h2[pair], pp_h1h2[pair]
      outfile.write(' '.join(pair))
      outfile.write(', ')
      outfile.write(' '.join([pair[1], prep, pair[0]]))
      outfile.write("\n")
      outfile.write("\t"+' | '.join(nn_h1h2[pair]))
      outfile.write("\n")
      outfile.write("\t"+' | '.join(pp_h1h2[pair][prep]))
      outfile.write("\n")

outfile.close()

print [p for p in sorted(altpreps.items(), key=itemgetter(1), reverse=True)]
    