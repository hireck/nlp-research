#!/usr/bin/env python 
# -*- coding: utf-8 -*-

'''Some tools for working with constituent trees'''

def is_leaf(subtree):
  '''determines if something is a leaf of the tree (contains no further internal structure)'''
  return subtree["tokenRange"]["begin"] == subtree["tokenRange"]["end"]


def find_text(tree, tokens, text):
  if is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    text.append(token["text"])
  else:
    for c in tree["children"]:
      find_text(c, tokens, text) #recursion step!
      
def get_text(tree, tokens):
  '''extracts the text from a sentence or constituent'''
  text = []
  find_text(tree, tokens, text) #use recursive function to walk the tree and collect all the text
  return ' '.join(text)  

def find_postags(tree, tokens, postags):
  if is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    postags.append(token["posValue"])
  else:
    for c in tree["children"]:
      find_postags(c, tokens, postags) #recursion step!
      
def get_postags(tree, tokens):
  '''extracts the pos tags from a sentence or constituent'''
  postags = []
  find_postags(tree, tokens, postags) #use recursive function to walk the tree and collect all the pos tags
  return ' '.join(postags)    
  
def find_concept(np, indices):
  if is_leaf(np) and "inConcept" in np and np["inConcept"] == "1":
    indices.append(np["tokenIndex"])
  elif "children" in np:
    for c in np["children"]:
      find_concept(c, indices) #recursion step!

def get_concept(np):
  indices = []
  find_concept(np, indices)
  return indices
  
def get_nps(tree, nps):
  '''extracts all noun phrases from a sentence or constituent'''
  if "type" in tree and tree["type"]=="NP":
    nps.append(tree)
  if "children" in tree: #replace 'if' by 'elif' to only get maximal noun phrases
    for c in tree["children"]:
      get_nps(c, nps) #recursion step!

def get_max_nps(tree, nps):
  '''extracts all noun phrases from a sentence or constituent'''
  if "type" in tree and tree["type"]=="NP":
    nps.append(tree)
  elif "children" in tree: #only get maximal noun phrases
    for c in tree["children"]:
      get_nps(c, nps) #recursion step!      
            
def top_structure(tree, tokens):
  '''lists the types or pos tags of the immediate children of a constituent'''
  labels = []
  if "children" in tree:
    for c in tree["children"]:
      if "type" in c:
        labels.append(c["type"])
      else:
        token_index = c["tokenRange"]["begin"]
        token = tokens[token_index]
        pos = token["posValue"]
        labels.append(pos)
  return labels
  

def n_head_from_leaf(np, tokens):
  token_index = np["tokenRange"]["begin"]
  token = tokens[token_index]
  pos = token["posValue"]
  if pos.startswith('N'): 
    head = token["lemma"]
    np["head"] = "np_head"
    np["inConcept"] = "1" #mark the head as part of the concept (within a possibly larger NP)
    return head
  
def np_head(np, tokens):
  '''returns the lemma of the head noun of a noun phrase'''
  if is_leaf(np):
    head = n_head_from_leaf(np, tokens)
    return head
  else:
    for c in reversed(np["children"]):
      if is_leaf(c):
        head = n_head_from_leaf(c, tokens)
        return head
      elif "type" in c and c["type"]=="NP":
        return np_head(c, tokens) #recursion step!

def add_leaf_info(tree, tokens): 
  '''adds token information to the parse tree'''
  if is_leaf(tree):
    token_index = tree["tokenRange"]["begin"]
    token = tokens[token_index]
    tree["text"] = token["text"]
    tree["lemma"] = token["lemma"]
    tree["posValue"] = token["posValue"]
    tree["tokenIndex"] = token_index
    #text.append(form)
    #lemmas.append(lemma)
    #postags.append(postag)
    #indices.append(token_index)
  elif "children" in tree:
      for c in tree["children"]:
        add_leaf_info(c, tokens)
  #else: print tree
