#!/usr/bin/env python 
# -*- coding: utf-8 -*-


from __future__ import division
import json
from pprint import pprint
import codecs
from collections import defaultdict
from operator import itemgetter
import fnmatch
import os
import gzip
from lxml import etree
from constit_tools import is_leaf, get_text, get_nps, top_structure, np_head 




      
nn_h1h2 = {}
pp_h1h2 = {}
prep_fd = defaultdict(int)
totnps = 0
	
	
matches = []
for root, dirnames, filenames in os.walk('/media/data/unsilo/sample-data'):
  for filename in fnmatch.filter(filenames, 'document-concepts-pre.json.gz'):
    matches.append(os.path.join(root, filename))  
    #print os.path.join(root, filename)
print len(matches)
for path in matches[:500]:
  with gzip.open(path) as f:
    data = json.load(f)
  try:  
    trees = data["sentenceConstituents"]
    sentences = data["sentences"]
  except KeyError: 
    print path
  for sent in trees:	
    s_index = sent["sentenceIndex"]
    tokens = sentences[s_index]["tokens"]
    nps = []
    get_nps(sent, nps)
    for np in nps:
      totnps += 1
      try:
	pattern = top_structure(np, tokens)
	if (pattern[-1] == 'NN' or pattern[-1] == 'NNS') and pattern[-2] == 'NN' and not 'CC' in pattern:
	  head1 = np_head(np, tokens)
	  if head1:
	    head2 = np_head(np["children"][-2], tokens)
	    if head2:
	      mods = []
	      for c in np["children"][:-2]:
		if is_leaf(c):
		  token_index = c["tokenRange"]["begin"]
		  token = tokens[token_index]
		  pos = token["posValue"]
		  if pos in ['NN', 'NNP']:#, 'JJ', 'VBN', 'VBG']:
		    c["type"] = pos
		    mods.append(get_text(c, tokens)) 
	      if not (head2, head1) in nn_h1h2:
		nn_h1h2[(head2, head1)] = defaultdict(list)
	      nn_h1h2[(head2, head1)][' '.join(mods)].append(get_text(np, tokens)) #add path(/docID/filename), s_index, tokenRange
	elif pattern == ['NP', 'PP']:
	  head1 = np_head(np, tokens)
	  if head1:
	    pp = np["children"][-1]
	    head2 = np_head(pp, tokens)
	    p_index = pp["tokenRange"]["begin"]
	    prep = tokens[p_index]["lemma"]
	    pos = tokens[p_index]["posValue"]
	    if pos in ['IN', 'TO', 'VBN', 'VBG']:
	      if tokens[p_index+1]["posValue"] == 'IN':
		preptext = '_'.join([tokens[p_index]["text"], tokens[p_index+1]["text"]])
	      #elif tokens[p_index+2]["posValue"] in ['IN', 'TO'] and tokens[p_index+1]["posValue"] in ['NN']: #to capture phrasal preps like 'in relation to', but generates too much noise
		#preptext = '_'.join([tokens[p_index]["text"], tokens[p_index+1]["text"], tokens[p_index+2]["text"]])
	      else:
		preptext = tokens[p_index]["text"]
	      prep_fd[preptext] += 1
	      if head2 and not (head2 in ['equation'] or head1 in ['equation']) and not preptext in ['than', 'since', 'following', 'outside']:
		mods = []
		sub_nps = []
		get_nps(pp, sub_nps)
		if len(sub_nps) > 1:
		  main_np = sub_nps[1]
		else: main_np = sub_nps[0]
		if "children" in main_np:
		  for c in main_np["children"][:-1]:
		    if is_leaf(c):
		      token_index = c["tokenRange"]["begin"]
		      token = tokens[token_index]
		      pos = token["posValue"]
		      if pos in ['NN', 'NNP']:#, 'JJ', 'VBN', 'VBG']:
			c["type"] = pos
			mods.append(get_text(c, tokens)) 
		if not (head2, head1) in pp_h1h2:
		  pp_h1h2[(head2, head1)] = {}
		if not preptext in pp_h1h2[(head2, head1)]:
		  pp_h1h2[(head2, head1)][preptext] = defaultdict(list)  
		pp_h1h2[(head2, head1)][preptext][' '.join(mods)].append(get_text(np, tokens)) #add path(/docID/filename), s_index, tokenRange
      except IndexError: 
	break 

print totnps
print len([pair for pair in nn_h1h2 if pair in pp_h1h2])
#print [p for p in sorted(prep_fd.items(), key=itemgetter(1), reverse=True)]

altpreps = defaultdict(int)
	
outfile = codecs.open('noun_alternations.xml', 'w', 'utf-8')
root = etree.Element("AlternatingConcepts")
for pair in nn_h1h2:
  if pair in pp_h1h2:
    concept = etree.SubElement(root, "Concept")
    concept.set("n_form", ' '.join(pair))
    for prep in pp_h1h2[pair]:
      altpreps[prep] += 1
      concept.set("p_form_"+prep, ' '.join([pair[1], prep, pair[0]]))
    if nn_h1h2[pair]['']:
      n_inst = etree.SubElement(concept, "N_Instances")
      for phrase in nn_h1h2[pair]['']:
	ex = etree.SubElement(n_inst, "Ex")
	ex.set("text", phrase)
    for prep in pp_h1h2[pair]:
      if pp_h1h2[pair][prep]['']:
        p_inst = etree.SubElement(concept, "P_Instances")
        p_inst.set("prep", prep)
        for phrase in pp_h1h2[pair][prep]['']:
          ex = etree.SubElement(p_inst, "Ex")
          ex.set("text", phrase)  
      shared_mods = []
      for mod in pp_h1h2[pair][prep]:
        if not mod == '':
          if mod in nn_h1h2[pair]:
            shared_mods.append(mod)
      if shared_mods:
        for mod in shared_mods:
          sub = etree.SubElement(concept, "SubConcept")
          sub.set("alternating", "1")
          sub.set("n_form", ' '.join([mod, pair[0], pair[1]]))
          sub.set("p_form_"+prep, ' '.join([pair[1], prep, mod, pair[0]]))
          n_inst = etree.SubElement(sub, "N_Instances")
          for phrase in nn_h1h2[pair][mod]:
            ex = etree.SubElement(n_inst, "Ex")
            ex.set("text", phrase)
          p_inst = etree.SubElement(sub, "P_Instances")
          for phrase in pp_h1h2[pair][prep][mod]:
            ex = etree.SubElement(p_inst, "Ex")
            ex.set("text", phrase)  
    for mod in nn_h1h2[pair]:
      if not mod in shared_mods and not mod == '':
        sub = etree.SubElement(concept, "SubConcept")
        sub.set("alternating", "0")
        sub.set("n_form", ' '.join([mod, pair[0], pair[1]]))
        n_inst = etree.SubElement(sub, "N_Instances")
        for phrase in nn_h1h2[pair][mod]:
            ex = etree.SubElement(n_inst, "Ex")
            ex.set("text", phrase)
    for prep in pp_h1h2[pair]:
      for mod in pp_h1h2[pair][prep]:
        if not mod in shared_mods and not mod == '':
          sub = etree.SubElement(concept, "SubConcept")
          sub.set("alternating", "0")
          sub.set("p_form_"+prep, ' '.join([pair[1], prep, mod, pair[0]]))
          p_inst = etree.SubElement(sub, "P_Instances")
          for phrase in pp_h1h2[pair][prep][mod]:
            ex = etree.SubElement(p_inst, "Ex")
            ex.set("text", phrase) 
        
outfile.write(etree.tostring(root))      
outfile.close()

print len([c for c in root.iter("Concept")])
print len([ex for ex in root.iter("Ex")])

print [p for p in sorted(altpreps.items(), key=itemgetter(1), reverse=True)]

    